<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CityTableSeeder::class);
        $this->call(CostTableSeeder::class);
        $this->call(FloorTableSeeder::class);
        $this->call(RoomTableSeeder::class);
        $this->call(SquareTableSeeder::class);
        $this->call(TypeRendTableSeeder::class);
    }
}
