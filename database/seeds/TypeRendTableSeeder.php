<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeRendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rends')->insert([
            'rend' => 'посуточно'
        ]);
        DB::table('rends')->insert([
            'rend' => 'почасовая'
        ]);
        DB::table('rends')->insert([
            'rend' => 'долгосрочная'
        ]);
    }
}
