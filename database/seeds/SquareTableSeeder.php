<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SquareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('squares')->insert([
            'square' => '20'
        ]);
        DB::table('squares')->insert([
            'square' => '25'
        ]);
        DB::table('squares')->insert([
            'square' => '30'
        ]);
        DB::table('squares')->insert([
            'square' => '35'
        ]);
        DB::table('squares')->insert([
            'square' => '40'
        ]);
        DB::table('squares')->insert([
            'square' => '45'
        ]);
        DB::table('squares')->insert([
            'square' => '50'
        ]);
        DB::table('squares')->insert([
            'square' => '55'
        ]);
    }
}
