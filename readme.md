1) Размещаем на сервере.
2) Пишем sudo chmod -R 777 /opt/lampp/htdocs/testtask1.
3) Переходми в папку "testtask1" cd /opt/lampp/htdocs/testtask1.
4) Пишем composer install.
5) Копируем из файла env.example в файл .env.
6) прописуем php artisan key:generate
7) В phpMyAdmin создаем базу "DB_testtask" выбираем кодироку 'utf8_general_ci'.
8) прописуем в файле настройки базы .env DB_DATABASE=DB_testtask, DB_USERNAME=root, DB_PASSWORD=
9) В терминал пишете php artisan migrate.
10) В терминал пишете php artisan db:seed.
11) Запускаем сервер.
12) Перходим на старновй адресс http://testtask1/.