<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
    .form-group {
        margin: 5px;
    }
    .header > li{
        float: left;
        margin: 10px 20px 10px 15px;
        list-style-type: none;
    }
</style>
<body>
<div class="container">
    <a href="{{ route('home') }}">Create</a> <a href="{{ route('search') }}">Search</a>
    <h2>Search</h2>
    <ul class="header">
        <li>Rend/Country</li>
        <li>Costs</li>
        <li>Squares</li>
        <li>Rooms</li>
        <li>Floors</li>
    </ul></br></br>
    <form method="post" action="{{ route('find') }}" class="form-inline">
        {{csrf_field()}}
        <div class="form-group">
            <select class="form-control" id="rend" name="rend">
                @if(isset($typeRends))
                    @foreach($typeRends as $rend)
                        <option>{{ $rend->rend }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="cost_1" name="cost_1">
                @if(isset($costs))
                    @foreach($costs as $cost)
                        <option>{{ $cost->cost }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="square_1" name="square_1">
                @if(isset($squares))
                    @foreach($squares as $square)
                        <option>{{ $square->square }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="rooms_1" name="rooms_1">
                @if(isset($rooms))
                    @foreach($rooms as $room)
                        <option>{{ $room->room }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="floors_1" name="floors_1">
                @if(isset($floors))
                    @foreach($floors as $floor)
                        <option>{{ $floor->floor }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <br>
        <div class="form-group">
            <select class="form-control" id="city" name="city">
                @if(isset($cities))
                    @foreach($cities as $city)
                        <option>{{ $city->city }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="cost_2" name="cost_2">
                @if(isset($costs))
                    @foreach($costs as $cost)
                        <option>{{ $cost->cost }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="square_2" name="square_2">
                @if(isset($squares))
                    @foreach($squares as $square)
                        <option>{{ $square->square }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="rooms_2" name="rooms_2">
                @if(isset($rooms))
                    @foreach($rooms as $room)
                        <option>{{ $room->room }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" id="floors_2" name="floors_2">
                @if(isset($floors))
                    @foreach($floors as $floor)
                        <option>{{ $floor->floor }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <hr>
</div>
<div class="container" id="container">
    @if(isset($houses))
        @foreach($houses as $house)
            <ul>
                <li>Name: {{ $house->name }}</li>
                <li>Cost: {{ $house->cost->cost }}</li>
                <li>Floors: {{ $house->floor->floor }}</li>
                <li>Rends: {{ $house->typeRend->rend }}</li>
                <li>Rooms: {{ $house->room->room }}</li>
                <li>Squares: {{ $house->square->square }}</li>
            </ul>
        @endforeach
        {{ $houses->links() }}
    @endif
</div>

</body>

</html>
