<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="{{ route('home') }}">Create</a> <a href="{{ route('search') }}">Search</a>
    <h2>Create</h2>
    <form method = "post" action="{{ route('save') }}">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name" >Select name</label>
            <input type="text" required class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="city">Select city</label>
            <select class="form-control" id="city" name="city">
                @if(isset($cities))
                    @foreach($cities as $city)
                        <option>{{ $city->city }}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="rend">Select rend</label>
            <select class="form-control" id="rend" name="rend">
                @if(isset($typeRends))
                    @foreach($typeRends as $rend)
                        <option>{{ $rend->rend }}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="cost">Select costs</label>
            <select class="form-control" id="cost" name="cost">
                @if(isset($costs))
                    @foreach($costs as $cost)
                        <option>{{ $cost->cost }}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="square">Select squares</label>
            <select class="form-control" id="square" name="square">
                @if(isset($squares))
                    @foreach($squares as $square)
                        <option>{{ $square->square }}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="rooms">Select rooms</label>
            <select class="form-control" id="rooms" name="rooms">
                @if(isset($rooms))
                    @foreach($rooms as $room)
                        <option>{{ $room->room }}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="floors">Select floors</label>
            <select class="form-control" id="floors" name="floors">
                @if(isset($floors))
                    @foreach($floors as $floor)
                        <option>{{ $floor->floor }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
