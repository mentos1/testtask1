<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 17.10.17
 * Time: 17:58
 */
namespace App\Repositories\House;

use App\City;
use App\Cost;
use App\Floor;
use App\House;
use App\Room;
use App\Square;
use App\TypeRend;
use Illuminate\Http\Request;

class EloquentHouseRepository implements  HouseRepository
{
    public function getAddingPage()
    {
        return [
            'cities' => City::all(),
            'costs' => Cost::all(),
            'floors' => Floor::all(),
            'rooms' => Room::all(),
            'squares' => Square::all(),
            'typeRends' => TypeRend::all()
        ];
    }

    public function save(Request $request)
    {
        $house = new House();
        $house->name = $request['name'];
        $house->id_rend = TypeRend::where('rend', '=', $request['rend'])->first()->id;
        $house->id_city = City::where('city', '=', $request['city'])->first()->id;
        $house->id_cost = Cost::where('cost', '=', $request['cost'])->first()->id;
        $house->id_floor = Floor::where('floor', '=', $request['floors'])->first()->id;
        $house->id_square = Square::where('square', '=', $request['square'])->first()->id;
        $house->id_room = Room::where('room', '=', $request['rooms'])->first()->id;
        $house->save();
    }

    public function search()
    {
        return [
            'cities' => City::all(),
            'costs' => Cost::all(),
            'floors' => Floor::all(),
            'rooms' => Room::all(),
            'squares' => Square::all(),
            'typeRends' => TypeRend::all(),
            'houses' => House::with(['city', 'typeRend', 'cost', 'square', 'room', 'floor'])->paginate(5)
        ];
    }

    public function find(Request $request)
    {
        $query = House::with(['city', 'typeRend', 'cost', 'square', 'room', 'floor']);

        if(isset($request['city']))
            $query = $query->whereHas('city', function ($query) use($request) {
                $query->where('cities.city', '=', $request['city']);
            });

        if(isset($request['rend']))
            $query = $query->whereHas('typeRend', function ($query) use($request) {
                $query->where('rends.rend', '=', $request['rend']);
            });

        if(isset($request['cost_1']) OR isset($request['cost_2'])) {
            $cost_1 = isset($request['cost_1']) ? $request['cost_1'] : 0;
            $cost_2 = isset($request['cost_2']) ? $request['cost_2'] : 0;
            $query = $query->whereHas('cost', function ($query) use($cost_1, $cost_2) {
                $query->whereBetween('costs.cost',
                    [
                        min([$cost_1, $cost_2]),
                        max([$cost_1, $cost_2])
                    ]
                );
            });
        }

        if(isset($request['square_1']) OR isset($request['square_2'])) {
            $square_1 = isset($request['square_1']) ? $request['square_1'] : 0;
            $square_2 = isset($request['square_2']) ? $request['square_2'] : 0;
            $query = $query->whereHas('square', function ($query) use($square_1, $square_2) {
                $query->whereBetween('squares.square',
                    [
                        min([$square_1, $square_2]),
                        max([$square_1, $square_2])
                    ]
                );
            });

        }

        if(isset($request['rooms_1']) OR isset($request['rooms_2'])) {
            $rooms_1 = isset($request['rooms_1']) ? $request['rooms_1'] : 0;
            $rooms_2 = isset($request['rooms_2']) ? $request['rooms_2'] : 0;
            $query = $query->whereHas('room', function ($query) use($rooms_1, $rooms_2) {
                $query->whereBetween('rooms.room',
                    [
                        min([$rooms_1, $rooms_2]),
                        max([$rooms_1, $rooms_2])
                    ]
                );
            });

        }

        if(isset($request['floors_1']) OR isset($request['floors_2'])) {
            $floors_1 = isset($request['floors_1']) ? $request['floors_1'] : 0;
            $floors_2 = isset($request['floors_2']) ? $request['floors_2'] : 0;
            $query = $query->whereHas('floor', function ($query) use ($floors_1, $floors_2) {
                $query->whereBetween('floors.floor',
                    [
                        min([$floors_1, $floors_2]),
                        max([$floors_1, $floors_2])
                    ]
                );
            });

        }

        return [
            'cities' => City::all(),
            'costs' => Cost::all(),
            'floors' => Floor::all(),
            'rooms' => Room::all(),
            'squares' => Square::all(),
            'typeRends' => TypeRend::all(),
            "houses" => $query->paginate(5)
        ];
    }
}