<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 17.10.17
 * Time: 17:58
 */
namespace App\Repositories\House;

use Illuminate\Http\Request;

interface HouseRepository
{
    public function getAddingPage();

    public function save(Request $request);

    public function search();

    public function find(Request $request);

}