<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Square extends Model
{
    protected $table = 'squares';

    public function squares() {
        return $this->belongsTo('App\House');
    }
}
