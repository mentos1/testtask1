<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $table = 'houses';
    public $timestamps = false;
    protected $fillable = ['id','id_city','id_rend','id_cost','id_square','id_room','id_floor'];

    public function city() {
        return $this->hasOne('App\City', 'id' , 'id_city');
    }

    public function typeRend() {
        return $this->hasOne('App\TypeRend', 'id' , 'id_rend');
    }

    public function cost() {
        return $this->hasOne('App\Cost', 'id' , 'id_cost');
    }

    public function square() {
        return $this->hasOne('App\Square', 'id' , 'id_square');
    }

    public function room() {
        return $this->hasOne('App\Room', 'id' , 'id_room');
    }

    public function floor() {
        return $this->hasOne('App\Floor', 'id' , 'id_floor');
    }
    
}

