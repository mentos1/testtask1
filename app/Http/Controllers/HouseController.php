<?php

namespace App\Http\Controllers;

use App\Repositories\House\HouseRepository;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    private $houseRepository;

    public function __construct(HouseRepository $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    public function index()
    {
        return view('index', $this->houseRepository->getAddingPage());
    }

    public function save(Request $request)
    {
        $this->houseRepository->save($request);

        return redirect()->route('home');
    }

    public function search(Request $request)
    {
        if ($request->isMethod('get')) {

            return view('search', $this->houseRepository->search($request));
        }
    }

    public function find(Request $request)
    {
        return view('search', $this->houseRepository->find($request));
    }
}
