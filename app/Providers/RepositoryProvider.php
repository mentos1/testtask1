<?php

namespace App\Providers;

use App\House;
use App\Repositories\House\EloquentHouseRepository;
use App\Repositories\House\HouseRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(HouseRepository::class, function($app) {
            return new EloquentHouseRepository(new House());
        });

    }
}
