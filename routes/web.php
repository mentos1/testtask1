<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HouseController@index')->name('home');
Route::post('/save', 'HouseController@save')->name('save');
Route::get('/search', 'HouseController@search')->name('search');
Route::post('/find', 'HouseController@find')->name('find');
